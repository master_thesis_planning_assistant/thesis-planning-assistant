\section{Anforderungsermittlung}
\label{ch:analyse}
In diesem Abschnitt werden die grundlegenden funktionalen und nicht funktionalen Anforderungen an den Planungsassistenten definiert. Für die Definition von Anforderungen gibt es verschiedenste Vorgaben und Empfehlungen. Der etablierte und immer noch benutzte IEEE Standard 830-1998 \cite{noauthor_ieee_1998} macht Angaben zum empfohlenen inhaltlichen Aufbau einer Spezifikation für Software und System Anforderungen. Es werden aber auch weniger standardisierte Vorlagen wie Volere \cite{robertson_mastering_2006} verwendet. Diese Arbeit orientiert sich grob an den Vorgaben des internationalen Standards ISO/IEC/IEEE 29148 \cite{noauthor_isoiecieee_2011}, der verschiedene existierende Standards vereint. Der beschriebene Standard enthält drei Dokumente und deren Gliederung: das \glqq Stakeholder requirements specification document (StRS)\grqq{}, das \glqq System requirement specification document (SyRS)\grqq{} und das \glqq Software requirement specification document (SRS)\grqq{}. Da es sich bei dieser Arbeit um eine wissenschaftliche Thesis handelt, weicht die Gliederung von den im Standard definierten Dokumenten ab. Die groben inhaltlichen Elemente der Dokumente finden sich jedoch in den einzelnen Kapiteln wieder. Kapitel \ref{ch:grundlagen} entspricht inhaltlich dabei der StRS, Kapitel \ref{ch:analyse} der SyRS und Kapitel \ref{ch:entwurf} der SRS.\par
Die Anforderungen in diesem Kapitel werden vor allem informell, in Form von natürlicher Sprache, als auch semi-formell, in Form von strukturierten Modellen (Diagrammen) spezifiziert. Es existieren auch formalere Ansätze, die auf dem ISO/IEC/IEEE 29148 Standard aufbauen und ein Framework bieten, um die Qualität der resultierenden Dokumente zu erhöhen \cite{ali_process_2018}. Bei dem in dieser Arbeit vorgestellten System handelt es sich um einen Prototyp mit klar definiertem Umfang, somit kommen solche formalen Methoden hier nicht zum Einsatz. Jarzebowicz et al. \cite{jarzebowicz_selecting_2017} haben 30 Techniken zur Dokumentation von Anforderungen in einer Feldstudie verglichen und festgestellt, dass Anwendungsfälle (Use-Cases), User Stories, Prototyping und Prozessmodellierung am häufigsten und erfolgreichsten zum Einsatz kommen. Daher werden sie in dieser Arbeit ebenfalls angewendet. Allerdings wird, statt den oft im agilen Umfeld genutzten User Stories, die im Standard ISO/IEC/IEEE 29148 beschriebene Dokumentation verwendet. Diese stützt sich auf die Definition von funktionalen und nicht funktionalen Anforderungen.\par
Ebenfalls werden keine Anwendungen wie JIRA\footnote{https://www.atlassian.com/de/software/jira} zur Verwaltung von Anforderungen eingesetzt. Solche Anwendungen werden in vielen Unternehmen gerade im agilen Umfeld für die Dokumentation und Verwaltung von Anforderungen verwendet \cite{behutiye_documentation_2020}. Dabei wird ein großer Fokus auf das kollaborative Arbeiten an und mit Anforderungen gesetzt, was hier aufgrund der Art der Arbeit nicht benötigt wird.\par
In Kapitel \ref{ch:analyse:allgemeine_anforderungen} werden die allgemeinen Anforderungen an das Gesamtsystem dokumentiert. Dafür wird zunächst ein Anwendungsfall beschrieben und anschließend werden die \mbox{funktionalen} und nicht funktionalen Anforderungen für diesen aufgestellt. Im Anschluss wird in Kapitel \ref{ch:analyse:komponenen_anforderungen} auf die aus den allgemeinen Anforderungen resultierenden Anforderungen für die einzelnen Komponenten des Planungsassistenten eingegangen.

\subsection{Anforderungen an das System}
\label{ch:analyse:allgemeine_anforderungen}
Der Planungsassistent, im Folgenden als System bezeichnet, besteht aus mehreren Teilsystemen. Im Rahmen dieser Arbeit sollen die Teilsysteme Cross-Plattform-App \ref{ch:analyse:cross_plattform_app_anforderungen}, Orchestrierungs-Service \ref{ch:analyse:orchestrierungs_service_anforderungen} und Routing-Apdapter \ref{ch:analyse:routing_adapter_anforderungen} umgesetzt werden. Weitere Teilsysteme stellen der Nutzerpräferenz-Service, ein GATSPTW-Solver und ein Service zum Bestimmen von Zeit- und Ortsinstanzen und Zuordnen eines Nutzen zu Mobilitätsoptionen, im Folgenden Aufbereitungs-Service genannt, dar.\par
Die Hauptforderung an das System ergibt sich aus dem Wunsch des Nutzers das System zu verwenden, um eine auf seine Nutzerpräferenzen angepasste Aktivitätenanordnung und den zugehörigen Mobilitätsplan für einen Tag zu erhalten. Eine solche Aktivitätenanordnung mit zugehörigem Mobilitätsplan wird im Folgenden als Aktivitätsplan bezeichnet. Dafür verwendet die Anwendung die Aktivitäten des Nutzers, welche entweder fix oder flexibel sein können. Eine fixe Aktivität hat einen festgelegten Zeitpunkt und Ort (im Folgenden Zeit- und Ortsinstanz), wohingegen eine flexible Aktivität an einem beliebigen Zeitpunkt und/oder Ort stattfinden kann.\par
Eine weitere Funktionalität des Systems ist die Unterstützung der Kalibrierung der Nutzerpräferenzen pro Routing-Service bzw. API. Einige Parameter der Nutzerpräferenzen, wie bspw. wie stark soll Linksabbiegen vermieden werden, haben in den verschiedenen Routing-Services verschiedene Wertebereiche und werden unterschiedlich gehandhabt. Dadurch ist eine Kalibrierung solcher weichen Parameter nötig, um auf den Nutzer angepasste Mobilitätsoptionen zu erhalten.\par
Im Folgenden sind die Hauptfunktionalitäten des Systems in Form eines Anwendungsfalls (Use-Cases) spezifiziert. 

\begin{enumerate}[label=\textbf{AF-\arabic*}]
    \setcounter{enumi}{9}
	\item\label{AF-10} Erstellen eines Aktivitätsplans
	\begin{enumerate}
	    \item Nutzer legt fixe und flexible Aktivitäten an.
	    \item System frage Präferenzen des Nutzers ab.
	    \item System berechnet Optionen für Zeit und Ort für flexible Aktivitäten.
	    \item System fragt verschiedene Mobilitätsoptionen anhand der Aktivitäten und der Nutzerpräferenzen ab.
	    \item System beurteilt und filtert die Mobilitätsoptionen anhand der Nutzerpräferenzen.
	    \item System errechnet eine Aktivitätenanordnung anhand der Mobilitätsoptionen und ihrer Beurteilung.
	    \item Nutzer sieht resultierende Aktivitätspläne ein.
	\end{enumerate}
\end{enumerate}

Aus den Anwendungsfällen ergeben sich die folgenden funktionalen Anforderungen. Eine detailliertere Auflistung der Anforderungen für die einzelnen Teilsysteme ist im Anhang \ref{appendix:detailierte_anforderungen} zu finden.

\begin{enumerate}[label=\textbf{FA-\arabic*}]
    \setcounter{enumi}{9}
	\item\label{FA-10} Das System soll dem Nutzer die Möglichkeit bieten, über eine Cross-Plattform-App feste und flexible Aktivitäten für einen Tag festzulegen.
	\setcounter{enumi}{19}
	\item\label{FA-20} Das System soll die vom Nutzer festgelegten Aktivitäten an den Orchestrierungs-Service übergeben, um einen Aktivitätsplan zu erhalten.
	\setcounter{enumi}{29}
	\item\label{FA-30} Das System soll die Präferenzen des Nutzers an einem Nutzerpräferenz-Service abfragen.
	\setcounter{enumi}{39}
	\item\label{FA-40} Das System soll anhand der Präferenzen und Aktivitäten über einen Serviceaufruf mögliche Zeit- und Ortsinstanzen für jede in Zeit und/oder Ort flexible Aktivität finden.
	\setcounter{enumi}{49}
	\item\label{FA-50} Das System soll anhand der Präferenzen und Aktivitäten mit Zeit- und Ortsinstanzen durch den Aufruf des Routing-Adapters mögliche Mobilitätsoptionen in Form von Routen zu den Aktivitäten finden.
	\setcounter{enumi}{59}
	\item\label{FA-60} Das System soll die gefunden Mobilitätsoptionen anhand der Präferenzen des Nutzers filtern.
	\setcounter{enumi}{69}
	\item\label{FA-70} Das System soll die gefundenen Mobilitätsoptionen über einen Serviceaufruf basierend auf den Präferenzen des Nutzers bewerten und einen Nutzen zuordnen.
	\setcounter{enumi}{79}
	\item\label{FA-80} Das System soll mithilfe eines GATSPTW-Solvers eine auf den Nutzer abgestimmte Anordnung der Aktivitäten zusammen mit den bewerteten Mobilitätsoptionen finden. 
	\setcounter{enumi}{89}
	\item\label{FA-90} Das System soll dem Nutzer die Möglichkeit bieten, den gefundenen Aktivitätsplan einzusehen.
	\setcounter{enumi}{99}
	\item\label{FA-100} Das System soll dem Nutzer die Möglichkeit bieten, die gefundenen Aktivitätspläne zu verwalten.
	\setcounter{enumi}{109}
	\item\label{FA-110} Das System soll die Nutzerpräferenzen eines Nutzers abrufen, um eine Kalibrierung von weichen Parametern durchzuführen.
	\setcounter{enumi}{119}
	\item\label{FA-120} Das System soll für die Nutzerpräferenzen eine Anfrage für Mobilitätsoptionen an den Routing-Adapter stellen, um Routen für die Kalibrierung zu erhalten.
\end{enumerate}

Da es sich bei dem System um einen Prototyp handelt, werden vor allem die nicht funktionalen Anforderungen Wartbarkeit bzw. Erweiterbarkeit, Skalierbarkeit, Portabilität und Performance betrachtet. Andere nicht funktionale Anforderungen wie Benutzbarkeit, Sicherheit und Verlässlichkeit stehen somit im Hintergrund.\par
Performance und Skalierbarkeit spielen eine große Rolle, da in dem System mit einem GATSPTW-Solver und Routinganfragen mehrere rechenintensive Prozesse kombiniert werden. Wie der Name bereits andeutet, erfordert die Cross-Plattform-App ein hohes Maß an Portabilität, aber auch die anderen Teilsysteme sollen möglichst plattformunabhängig gehalten werden. In Bezug auf die Erweiterbarkeit stellt vor allem der Routing-Adapter eine wichtige Komponente dar. Er soll zum einen die Erweiterbarkeit des Einsatzgebietes des Systems sicherstellen, indem neue Routing-Services und Verkehrsverbünde angebunden werden können. Zum anderen soll das Hinzufügen neuer Nutzerpräferenzparameter und das Abbilden dieser auf die Parameter der Routing-Services ohne direkte Anpassungen am Code möglich sein.

\begin{enumerate}[label=\textbf{NF-\arabic*}]
    \setcounter{enumi}{9}
	\item\label{NF-10} Das System soll Anfragen wo möglich asynchron und parallelisiert stellen, um Performanceeinbußen durch Engpässe zu vermeiden. 
	\setcounter{enumi}{19}
	\item\label{NF-20} Das System soll wo möglich mehrfach benötigte Daten, wie bspw. kalibrierte Parameter, ab- und zwischenspeichern, sodass es bei Planungsanfragen nicht zu Performanceeinbußen durch redundante Berechnungen kommt.
	\setcounter{enumi}{29} 
	\item\label{NF-30} Das System soll durch Mehrfachinstanzierung der einzelnen Komponenten und Loadbalancing skalierbar sein. 
	\setcounter{enumi}{39}
	\item\label{NF-40} Das System soll durch eine entsprechende Technologiewahl möglichst plattformunabhängig sein, um Portabilität zu ermöglichen. 
	\setcounter{enumi}{49}
	\item\label{NF-50} Das System soll durch die Wahl einer angemessenen Architektur, Dokumentation und Codequalität wartbar und erweiterbar sein. 
\end{enumerate}

\clearpage
\subsection{Anforderungen an die Komponenten}
\label{ch:analyse:komponenen_anforderungen}
Aus den in Kapitel \ref{ch:analyse:allgemeine_anforderungen} beschriebenen Anforderungen für das Gesamtsystem des Planungsassistenten ergeben sich Anforderungen für die einzelnen Komponenten des Systems. Zuerst werden in Kapitel \ref{ch:analyse:cross_plattform_app_anforderungen} die Anforderungen an die Cross-Plattform-App definiert. Anschließend beschreibt Kapitel \ref{ch:analyse:orchestrierungs_service_anforderungen} welche Anforderungen an den Orchestrierungs-Service gestellt werden. Abschließend sind in Kapitel \ref{ch:analyse:routing_adapter_anforderungen} die Anforderungen an den Routing-Adapter aufgeführt. Für alle drei Komponenten ist zudem eine formale und detaillierte Auflistung der Anforderungen im Anhang in Kapitel \ref{appendix:detailierte_anforderungen} zu finden.\par
 
\subsubsection{Anforderungen an die Cross-Plattform-App}
\label{ch:analyse:cross_plattform_app_anforderungen}
Die Cross-Plattform-App, im folgenden Anwendung, stellt das Frontend dar und bietet dem Nutzer die Möglichkeit, mit dem System zu interagieren, Aktivitäten festzulegen, aus ihnen Aktivitätspläne erstellen zu lassen und diese zu verwalten. Zudem soll der Nutzer seine angegebenen Nutzerpräferenzen einsehen können. Die Umsetzung als Cross-Plattform-App soll eine Nutzung als App sowohl auf Android als auch auf iOS basierten Mobilgeräten ermöglichen. In dem in dieser Arbeit entwickelten Prototypen steht hierbei Android im Fokus, da für das Kompilieren und Testen einer App für iOS ein macOS Gerät benötigt wird.\par
In Abbildung \ref{fig:analyse_cross_plattform_app_process} ist der Prozess für die Anfrage eines Aktivitätsplans über die Cross-Plattform-App dargestellt. Dabei beginnt der Nutzer mit der Auswahl eines Tages. Anschließend hat er die Möglichkeit Aktivitäten aus seinem Kalender in die Anwendung zu importieren oder diese manuell in der Anwendung anzulegen. Hat der Nutzer alle gewünschten Aktivitäten für den Tag eingetragen, kann er diese bestätigen und Aktivitätspläne am Orchestrierungs-Service anfragen. Im Anschluss werden dem Nutzer mehrere Optionen in Form von möglichen Aktivitätsplänen angezeigt. Der Nutzer hat die Möglichkeit, eine der Optionen auszuwählen und abzuspeichern oder alle zu verwerfen.\par
Eine detaillierte Auflistung der funktionalen und nicht funktionalen Anforderungen an die Cross-Plattform-App ist im Anhang \ref{appendix:detailierte_anforderungen:cross_plattform_app} zu finden.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/03_app_process.pdf}
    \caption{Prozess in der Cross-Plattform-App}
    \label{fig:analyse_cross_plattform_app_process}
\end{figure}

\subsubsection{Anforderungen an den Orchestrierungs-Service}
\label{ch:analyse:orchestrierungs_service_anforderungen}
Der Orchestrierungs-Service stellt die zentrale Komponente des Systems dar. Zum einen repräsentiert er die Backend-Schnittstelle für die Cross-Plattform-App. Zum anderen regelt er das Aufrufen der verschiedenen Teilsysteme und die Aggregierung der resultierenden Daten.\par
In Abbildung \ref{fig:analyse_orchestration_service_process} sind die Prozesse für die Beantwortung einer Anfrage für einen Aktivätsplan dargestellt. Für das Erstellen eines Aktivitätsplans fragt der Orchestrierungs-Service zuerst die Präferenzen des Nutzers am Nutzerpräferenz-Service ab. Das Präferenzprofil eines Nutzers besteht dabei aus statischen Parametern, die für alle Modale und Routing-Services gleich sind und dynamischen Parametern, die sich basierend auf verschiedenen Faktoren wie bspw. dem Modal unterscheiden können. Anschließend werden die benötigten Zeit- und Ortsinstanzen für die Aktivitäten an einem Service angefragt. Dadurch werden allen flexiblen Aktivitäten mehrere Zeit- und/oder Ortsinstanzen zugewiesen und es findet eine Vorselektion der Anfragen an die Routing-Services statt. Mit den resultierenden Aktivitäten mit Zeit und Ort werden, zusammen mit den Nutzerpräferenzen, Mobilitätsoptionen am Routing-Adapter angefragt. Die erhaltenen Mobilitätsoptionen werden dann hinsichtlich ihres Nutzens basierend auf den Nutzerpräferenzen beurteilt und gefiltert. Schließlich kann an einem GATSPTW-Solver eine Aktivitätenanordnung anhand der Aktivitäten und Mobilitätsoptionen abgefragt werden.\par
Für die Kalibrierung der Parameter wird zuerst mithilfe heuristischer Approximationsverfahren eine Parametrisierung für die Nutzerpräferenzen des Nutzers festgelegt. Anschließend werden im Routing-Adapter Routen mit diesen Nutzerpräferenzen berechnet. Die resultierenden Routen werden zusammen mit den Parameter der Nutzerpräferenzen analysiert, um diese entsprechend zu kalibrieren und nach mehreren Iterationen eine möglichst gute Parametrisierung zu finden. Diese Kalibrierung muss pro (dynamischem) Präferenzprofil durchgeführt werden.\par
Eine detaillierte Auflistung der funktionalen und nicht funktionalen Anforderungen an den Orchestrierungs-Service ist im Anhang \ref{appendix:detailierte_anforderungen:orchestrierungs_service} zu finden.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/03_orchestration_process.pdf}
    \caption{Prozess im Orchestrierungs-Service}
    \label{fig:analyse_orchestration_service_process}
\end{figure}

\subsubsection{Anforderungen an Routing-Adapter}
\label{ch:analyse:routing_adapter_anforderungen}
Der Routing-Adapter stellt eine gemeinsame Schnittstelle für die verschiedenen Routing-APIs und Services dar. Er verarbeitet eine Anfrage, indem die Nutzerpräferenzen eines Nutzers für die Routinganfragen auf die verschiedenen Parameter der Routing-APIs und Services abgebildet werden, um möglichst gute Mobilitätsoptionen für den Nutzer zu erhalten.\par
In Abbildung \ref{fig:analyse_routing_adapter_process} ist der Prozess für die Beantwortung einer Routinganfrage abgebildet. Zuerst werden die relevanten Routing-APIs und Services anhand der Nutzerpräferenzen ausgewählt. Fährt der Nutzer bspw. nicht mit der Bahn, ist eine Anfrage an einer ÖPNV-API (öffentlicher Personen Nahverkehr) nicht erforderlich. Anschließend werden für jede Routing-API und Service die Nutzerpräferenzen auf die entsprechenden Routingparameter abgebildet. Mit den resultierenden Routingparametern wird der entsprechende Service bzw. die entsprechende API angefragt. Die erhaltene Routing-Antwort wird dann in das allgemeine Routenmodell transformiert, welches auch vom Orchestrierungs-Service und der Cross-Plattform-App verwendet wird. Anhand der transformierten Routen können Duplikate entfernt, die Routen konsolidiert und schließlich die Routinganfrage beantwortet werden.\par 
Eine detaillierte Auflistung der funktionalen und nicht funktionalen Anforderungen an den Routing-Adapter ist im Anhang \ref{appendix:detailierte_anforderungen:routing_adapter} zu finden.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/03_routing_adapter_process.pdf}
    \caption{Prozess im Routing-Adapter}
    \label{fig:analyse_routing_adapter_process}
\end{figure}